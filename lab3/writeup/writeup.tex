\documentclass[12pt]{article}
\usepackage{times}
\usepackage{color}
\newcommand{\code}[1]{\indent\textcolor{blue}{\ \ \ \textbf{julia\textgreater}\ #1}\\}

\begin{document}
\section*{Problem 1: github}
\subsection*{1a: Setting up github}
Done. Repository at http://github.com/alexrichert/astro585/

\subsection*{1b: Adding files to github}
Done.

\section*{Problem 2: File I/O}
\subsection*{2a: STDOUT with a vector}
Printing the array of N = 1024 took about 0.02 seconds the first time, and 0.003 seconds thereafter.

\subsection*{2c: Timing}
The first function for writing the array to a file took 0.008 seconds the first time, and 0.002 seconds thereafter.

For fun, I tested the write time of my other function (the one where input is by hand; I of course did not include the time taken to type in the values). For three values ([`1',`2',`3']), it took 0.0003 seconds to run. Hence, the size of our output matters (no surprise there).

The read time for the 1024-element array, calculated with \\
\code{@time readfile(``problem2\_myvector\_ascii.dat")}
was 5e-5 seconds.

To see how this compared with file read time outside Julia, I re-wrote the data file (with a new set of random numbers) and ran the shell command `md5sum' on it a few times, prefixed by the `time' command to time it. The hard drive read time appeared to make a neglible difference. This may be in part due to the fact that my home computer has a hybrid hard drive, so the file may be getting read from the solid state cache, so the timing difference is probably only apparent for large files.

So I will test this. I will change N from 1024 to 1 million, creating a 19\,MB ASCII file. The write time for the file was about 0.33 seconds, but the read time according to time/md5sum stayed constant as I kept re-running the command. Therefore, my hypothesis about a speedy hard drive was wrong, and it is simply that the file is still cached. Unable to figure out how to safely clear my system cache, I used Julia to create an enormous array that took up all of my system memory, thereby clearing the cache. Finally, I obtained the expected result: 0.4 seconds on the first try, and 0.05 seconds thereafter. I repeated this process in Julia; interestingly, Julia seemed to read the 19\,MB file about 8 times faster (i.e. the `md5sum' result implied a read time of nearly 0.4 seconds, whereas the Julia read time on the first load was just under 0.01 seconds).

\subsection*{2d: Binary files}
I wrote the function `writebinary()' to write to a binary file, which took 0.0002 seconds for an array size of 1024, and 0.03 seconds for 1 million. I checked the file type of the output with the shell `file' command, which was `data'. Happily, I was able to open the file in Python with the numpy module's `fromfile' command, which is good for future reference.

Opening the binary file containing 1024 numbers took 0.004 seconds the first time, and 6e-5 thereafter (compared with 5e-5 for ASCII, so not much of a difference). Surprisingly, the larger file (1e6 numbers) took only 0.17 seconds to load the first time, and 0.005 thereafter. The write, first read, and second read times for an array of 100 million (which took up a file that would fill a CD-ROM) were 0.9 seconds, 0.005 seconds, and 1e-4 seconds, respectively. Strangely, on the first read, the function took a noticeable amount of time to load the file (more than 3 seconds), but the result was only 0.005 seconds. This makes me think that Julia is somehow not timing the file opening properly. This is odd, given that the first run is still slower, but does not seem to measure the full function run time. Upon further investigation, it appears that the observed time difference has to do with code compilation rather than read time (it is only slower the first time running the function, but not the first time reading a newly written file). In any case, Julia seems to be ignoring read time (I could make semi-educated guesses about how Julia samples computation time and why that is a problem, but there is no need to digress further; the upshot is that it does not work).

\subsection*{2e: HDF5}
First, I will point out that when I ran `Pkg.add(``HDF5")', Julia automatically asked for my sudo password and ran `yum' to install the main HDF5 package for the system. I did not work perfectly since I had to also install the devel package, but that is still awesome.

For this problem, I used the `@save' and `@load' convenience functions; I put these inside my own functions which was a bit silly, but no harm done. Both reading and writing the 1024-element array took about 0.03 seconds, while taking about 0.5 seconds for N = 1 million (though there are still some issues with timing that make me wonder about the validity of these numbers). For N = 1 million, the file sizes for ASCII, binary, and HDF5 were 181\,MB, 77\,MB, and 77\,MB, respectively. Meanwhile, I successfully loaded the HDF5 file into Python with the `h5py' package. Hazah.


\section*{Problem 3: Tests}
\subsection*{3a,b,c: Checking functions from problem 2}
For the above write/read functions associated with my array of random Float64's, only the write functions have input parameters (only one--- the array to be written). The write functions do not return results, therefore I will only worry about the results of the three read functions (for ASCII, binary, and HDF5 files). Since each function should be returning the same thing--- an array of Float64's--- I have implemented a single function that checks the output of a given function (one of the three aforementioned read functions) and ensures that it is of type `Array{Float64,1}'. In this testing function, I also implement an option for deliberately breaking the output, by converting to an array of Float32's, thereby showing how the testing code would behave if the function itself were misbehaving (that is, misbehaving in a way that did not throw an error). I also implemented another function that checks whether each element in the output is finite, throwing an error when it finds a NaN or Inf. This function also has an option for deliberate breakage. This one ran much quicker than expected, so it may not have been necessary to put it in a separate function after all.

My three read functions pass these tests, meaning that both the write and read functions are behaving as they should, apart from the possibility that there are indeed numbers in the output files that can be taken in by the read functions, but perhaps not the right numbers (maybe they all get turned to zeros because I read them as binary instead of ASCII, for instance). Checking for problems here will be application dependent; in this case, since the numbers are randomly generated on a normal distribution with $\sigma^2=1$, I can ensure that their variance is within 1\% of the expected value using `@test\_approx\_eq\_eps(var(myoutput),1.0,0.01)'. Sure enough, the test passes for all three functions, meaning that all read and write functions are fully operational.

\subsection*{3d: Variance}
I used my array of $10^7$ normally-distributed random numbers from problem 2 to test with function `var\_compare', which uses my one-/two-pass variance functions from homework 1. I used Julia's `isfinite' function to determine whether the output of the one-pass algorithm was junk, in which case it reverted to the two-pass (as in the case where the mean is very large). This does not necessarily mean that the one-pass algorithm is ``safe," in the sense that it could still return a numerical result that is wildly inaccurate. I found that the one-pass algorithm started spitting out NaNs/Infs for mean values of $10^{148}$, with $N=10^7$ and a variance of unity. The one-pass result becomes 10\% inaccurate at a mean value of approximately $10^6$, as opposed to $10^{16}$ for the two-pass.

\subsection*{3e: Checking the new variance function}
I implemented function `checkvar\_compare', which implements pre- and post-conditions for the new variance function. First, the sub-function `checkarray' converts the input array to Float64 (which it should already be) and ensures finite elements (no NaNs or $\pm$Infs). At first I used the @test function to do this (with `isfinite'), but this turned out to be much slower (by about 6 seconds) than simply using an if statement which returns a NaN if the finiteness check fails. The function then runs the main variance function and stores the result. It then checks the finiteness of this result with `@test'.

\subsection*{3f: Try/catch}
I implemented a try-catch statement in the function `checkarray' (which also contains the standard if statement-based finiteness check), though slightly inelegantly, using an @assert check with the `isfinite' function, once again iterating over each element in the array whose variance we want.

\subsection*{3g: Performance}
Interestingly, checking the finiteness of each element in the array of interest added very little computational time to the `var\_compare' function. For a given array, the original variance algorithms (one-/two-pass) and the new `var\_compare' function (with array-checking) each took about the same amount of time to run (0.025 seconds, based on the array of $N=10^7$). The if statement-based and try-catch finiteness checks took the same amount of time to run.

\subsection*{3h: Checking for NaNs, yea or nay?}
As mentioned above, looping over the `@test' function with the `isfinite' check was extremely computationally expensive compared with a simple if statement-based or try-catch check. This latter check added negligible overhead, so especially in coding/testing/debugging phases, there is no reason to not include it (since I ran the loop for a quite large number of iterations, $10^7$).


\section*{Problem 4: Documentation}
\subsection*{4a: Commenting}
Done. I tried to write helpful comments, but also did not want to be overly verbose. Most functions only required one or two short comments, the bulk of the code is relatively simple and self-explanatory.
\subsection*{4b: Double check}
Done. Happily, I tested my code pretty frequently while I was writing and did the assignment linearly, so there were no issues of breakage.

\end{document}
