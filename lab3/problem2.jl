#problem 2a:
N = 10000000
myvector = randn(N)

#problem 2b:
function writefile(outputvector)
        myfile = open("problem2_myvector_ascii.dat","w")
        @printf(myfile,"%s",outputvector)
        close(myfile)
end

function writefrominput(printtime=0)
        myvector = [string()]
        print("Input values (type DONE to stop):\n")
        while true
                myinput = readline()
                myinput = myinput[1:length(myinput)-1]
                if myinput == "DONE"
                        break
                else
                        append!(myvector,[myinput])
                end
        end
        function timefunc()
                myfile = open("problem2_myvector_input_ascii.dat","w")
                @printf(myfile,"%s",myvector[2:]) #skipping first cell, which is blank
                close(myfile)
        end
        if printtime==1
                @time timefunc()
        else
                timefunc()
        end
end

function readfile()
        myfile = open("problem2_myvector_ascii.dat","r")
        inputvector = Float64[]
        while !eof(myfile)
                nextpiece = float64(readline(myfile))
                append!(inputvector,[nextpiece])
        end
        close(myfile)
        return inputvector
end

function writebinary(outputvector)
        myfile = open("problem2_myvector_binary.dat","w")
        write(myfile,outputvector)
        close(myfile)
end

function readbinary()
        myfile = open("problem2_myvector_binary.dat","r")
        inputvector = Float64[]
        while !eof(myfile)
                nextpiece = read(myfile,Float64)
                append!(inputvector,[nextpiece])
        end
        close(myfile)
        return inputvector
end

using HDF5, JLD

function writehdf5(outputvector)
        @save("problem2_myvector.hdf",outputvector)
end

function readhdf5()
        inputvector = @load("problem2_myvector.hdf",myvector)
        return inputvector
end
