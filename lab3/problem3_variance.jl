# This file contains everything related to problems 3d-h.
using Base.Test


# One-pass variance:
function var_onepass(y::Array)
        N = length(y)
        sum = zero(y[1])
        sum2 = zero(y[1])
        for i in 1:N
            sum += y[i]
            sum2 += y[i]^2.
        end
        return (sum2 - (sum*sum)/N)/(N-1)
end


# Two-pass variance
function var_twopass(y::Array)
        m = mean(y)
        N = length(y)
        sum2 = zero(y[1])
        for i in 1:N
            sum2 += (y[i]-m)^2.
        end
        return sum2/(N-1)
end


# Try one-pass, then two-pass if the result is nonsense:
function var_compare(y::Array,method="if")
        onepassresult = var_onepass(y)
        if isfinite(onepassresult)
                print("Using one-pass\n")
                return onepassresult
        else
                print("Using two-pass\n")
                return(var_twopass(y))
        end
end


# This function is used in the proceeding function to check array for NaNs/Infs:
function checkarray(y,method="if")
        if method=="if"
                for i in 1:length(y)
                        if !isfinite(y[i])
                                error("Bad array!")
                        end
                end
        elseif method=="try"
                for i in 1:length(y)
                        # This is not the most elegent way of implementing a try/catch statement,
                        #  but it at least lets me try it out in Julia for the first time.
                        try
                                @assert isfinite(y[i])
                        catch
                                error("Bad array!")
                        end
                end
        end
end


# A regression test for `var_compare()':
function checkvar_compare(y::Array)
        # Pre-conditions:
        y = float64(y)
        checkarray(method)
        # Run the function:    
        varresult = var_compare(y)
        # Post-conditions:
        @test isfinite(varresult)
end
