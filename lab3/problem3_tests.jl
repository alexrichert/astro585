include("problem2.jl")
using Base.Test

# This function gets handed one of the read-related functions
#  from problem 2 (`readfile',`readbinary',`readhfd5') and
#  ensures that the function's output is an array of Float64's.
function checktype(myfunc,breakbadly=false)
        myoutput = myfunc()
        if breakbadly # Guarantee that our test will fail
                myoutput = float32(myoutput)
        end
        @test typeof(myoutput)==Array{Float64,1}
        print("Success!")
end

# This function does similarly to `checktype()', except that it
#  iterates over each value of the output array and looks for NaNs/Infs.
function checknans(myfunc,breakbadly=false)
        myoutput = myfunc()
        if breakbadly
                myoutput[10]=NaN # Guarantee that our test will fail on tenth array element
        end
        for i in 1:length(myoutput)
                @test isfinite(myoutput[i])
        end
        print("Success!")
end

# This function uses the fact that we know that our numbers should
#  follow a normal distribution:
function checkvar(myfunc,breakbadly=false)
        myoutput = myfunc()
        if breakbadly
                myoutput *= 3.0
        end
        @test_approx_eq_eps(var(myoutput),1.0,0.01)
        print("Success!")
end
